#!/usr/bin/env bash

set -e

sudo apt-get update

sudo apt-get install -y sudo ufw
sudo ufw allow 22/tcp
sudo ufw logging off
yes | sudo ufw enable

sudo apt-get install -y emacs24 nano mg tmux netcat wget gcc g++ binutils make bc xauth \
     cscope git tig gitk \
     binutils-arm-none-eabi gcc-arm-none-eabi gdb-arm-none-eabi \
     qemu qemu-utils qemu-system-arm \
     openocd \
     linaro-boot-utils linaro-image-tools

sudo apt-get install -y libc6-dev
sudo apt-get install -y libc6-dev-i386

mkdir -p tmp build

cat <<EOF > ~/.emacs
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq-default indent-tabs-mode 'nil)
(setq-default tab-always-indent 'complete)
(setq-default tab-width 4)
(setq c-default-style "linux" c-basic-offset 4)
EOF


# linaro QEMU:
sudo apt-get build-dep -y qemu
git clone git://git.linaro.org/qemu/qemu-linaro.git ~/build/qemu-linaro
cd ~/build/qemu-linaro

git checkout 72f7eb07b611766298fe2dc140533a0a6256b054
rm include/libfdt_env.h # use system files instead!

./configure --target-list=arm-softmmu --prefix=$HOME/qemu \
            --disable-docs --disable-spice \
            --extra-cflags="-Wno-unused-local-typedefs -Wno-error=deprecated-declarations"

make && make install


# STH
git clone https://bitbucket.org/sicssec/sth.git build/sth
# git clone https://bitbucket.org/sicssec/sth-linux-stable.git build/sth-linux-stable

git -C build/sth fetch --all
# git -C build/sth-linux-stable fetch --all
