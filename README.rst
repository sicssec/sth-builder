STH builder
===========

Virtual machine configuration for building STH and related software.
This provides a standard build enviorment for STH.


Instructions
------------

To setup, you need to install vagrant and virtualbox. On Ubuntu this means::

    sudo apt-get install -y vagrant virtualbox virtualbox-ext-pack
    sudo usermod -G vboxusers -a $USER

OSX and Windows users need to install vagrant manually: https://www.vagrantup.com/downloads.html

After that, create and start the build machine::

    vagrant up

To log into the machine and to compile the code::

    vagrant ssh -- -c none
    [you will now log into the VM]
    cd build/sth
    make
    ...
    exit

To transfer data to/from the VM::

    # from your machine to the VM
    cp stuff shared/
    # from the VM to your machine
    cp stuff /vagrant/shared/


You can put the VM into sleep and wake it up later::

    vagrant suspend
    [....]
    vagrant resume

To completely remove the machine::

    vagrant destroy
    rm -rf .vagrant

Note that this remove all your VM files except anything in the shared folder.
